package org.weso.graph

/* Graphs are represented as 
 * Functional Inductive Graphs 
 * following Martin Erwig approach 
 *  Two key differences:
 *    Edges can also be nodes, so the type will be the same 
 *    Edges/Nodes have a unique ID so there is no need for separate Node Id's
 *    
 *    */

case class Arrow[A]
		(edge : A,    
		 node : A
		) {
}

case class Context[A]
		(node: A,
		 pred : Seq[Arrow[A]],  
		 succ : Seq[Arrow[A]],
		 nodes : Seq[(A,A)]
		)  
		 {
  def remove(n : A) : Context[A] = {
    Context(node,
            pred.filterNot(a => a.node == n || a.edge == n),
            succ.filterNot(a => a.node == n || a.edge == n),
            nodes.filterNot(p => p._1 == n || p._2 == n))
  }
  
  def insertPred(e: A, n : A) : Context[A] = 
    Context(node,pred :+ Arrow(edge = e,node = n),succ,nodes)
  
  def insertSucc(e : A, n : A) : Context[A] =
    Context(node,pred,succ :+ Arrow(edge = e,node = n),nodes)

  def insertNodes(x : A, y : A) : Context[A] =
    Context(node,pred,succ,nodes :+ (x,y))

  def triples : Seq[(A,A,A)] = {
    succ.map(s => (node,s.edge,s.node)) ++
    pred.map(p => (p.node,p.edge,node)) ++
    nodes.map(n => (n._1,node,n._2))
  }
}

trait IndGraph[A] {
  
  def isEmpty : Boolean
  def nodes : Seq[A]
  def matchGraph(node: A) : Option[(Context[A],IndGraph[A])]
  def insert(triple : (A,A,A)) : IndGraph[A]

  // Some functions on graphs
  def contains(node : A) : Boolean = {
    this.nodes.contains(node)
  }
  
  def triples : Seq[(A,A,A)] = {
    if (isEmpty) Seq()
    else {
     matchGraph(nodes.head) match {
      case None => Seq()
      case Some(p) => p._1.triples ++ p._2.triples
     }
    }
  }

}

case class MapGraph[A](map: Map[A,Context[A]]) extends IndGraph[A] {
  
  def isEmpty = map.isEmpty
  def nodes : Seq[A] = map.keys.toSeq
  
  def matchGraph(node : A) = {
    for {
      ctx <- map.get(node)
    } yield (ctx,this.cleanContext(node,ctx))
  }

  def insert(triple : (A,A,A)) : IndGraph[A] = {
    val (x,p,y) = triple
    val g1 = insertNode(x)
    val g2 = g1.insertNode(p)
    val g3 = g2.insertNode(y)
    g3.insertEdge(x,p,y)
  }
  
  def insertNode(node : A) : MapGraph[A] = {
    if (map.contains(node)) this
    else {
     MapGraph(map + (node -> Context(node,Seq(),Seq(),Seq())))
    } 
  }

  
  private def insertEdge(x: A, p: A, y: A) : MapGraph[A] = {
    // TODO: review safety...
    // this method supposes that (x,p,y) are already nodes of graph
     val cx1 = map(x).insertSucc(p,y)
     val m1 = map updated (x,cx1)
     
     val cy1 = m1(y).insertPred(p,x)
     val m2 = m1 updated (y,cy1)

     val cp1 = m2(p).insertNodes(x,y)
     val m3 = m2 updated (p,cp1)
     MapGraph(m3)
  }

  private def cleanContext(node: A, ctx : Context[A]) : MapGraph[A] = {
    MapGraph(map.mapValues(_.remove(node)) - node)
  }
}

object IndGraph {

  case class Empty[A]() extends IndGraph[A] {
  def isEmpty = true
  def nodes = Seq()
  def matchGraph(node : A) = None
  def insert(triple : (A,A,A)) : IndGraph[A] = {
    val g = MapGraph[A](Map.empty)
    g.insert(triple)
  }
}

  val g1 = MapGraph[Int](Map.empty)
  
  val g2 = g1.insert(1,2,3)
  val g3 = g2.insert(3,4,5)
  val g4 = g2.insert(1,6,5)
  
  println("Remove 5 : " + g4.matchGraph(5))
  println("Remove 6 : " + g4.matchGraph(5))
  
}