package org.weso.graph

import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.edge.LDiEdge
import scalax.collection.edge.Implicits._
import scalax.collection.edge.LBase.LEdgeImplicits
import scala.collection.Set
import org.weso.rdfNode._

// The following code is just to obtain the label as IRI object instead of LDiEdge#L1
// There may be a more simple way to do it
case class Label(val iri: IRI)
import scalax.collection.edge.LBase.LEdgeImplicits
object MyImplicit extends LEdgeImplicits[Label]
import MyImplicit._
// 

case class RDFGraph(graph : Graph[RDFNode,LDiEdge]) {
  
  def insertTriple(triple: (RDFNode,IRI,RDFNode)) : RDFGraph = {
    val (x,p,y) = triple 
    RDFGraph(graph + (x ~+> y)(Label(p)))
  }
  
  def addTriples(triples : Seq[(RDFNode,IRI,RDFNode)]) : RDFGraph = {
    triples.foldLeft (RDFGraph.empty) ((g,triple) => g.insertTriple(triple))
  }
  
  def findEdges(x :RDFNode, y: RDFNode) = {
    graph.edges filter ((e) => e._1 ==x && e._2 == y)
  }
  
  def triples : Set[(RDFNode,IRI,RDFNode)] = {
    for { 
      x <- graph.nodes
      y <- x.diSuccessors
      edge <- findEdges(x,y) 
    } yield (x.value,edge.value.iri,y.value) 
  }
  
}

object RDFGraph {
  def empty : RDFGraph = RDFGraph(Graph[RDFNode,LDiEdge]())
}

object GraphCollection extends App {
  val g = Graph[Int,LDiEdge]()
  val g1 = g + ((1 ~+> 2)('a'))
  val g2 = g1 + ((1 ~+> 3)('b'))
  val g3 = g2 + ((3 ~+> 1)('c'))
  println(g3)
  println("Successors of 3: " + g3.get(3).diSuccessors )
  println("Predecessors of 3: " + g3.get(3).diPredecessors )
  println("Nodes linked by a: " + (g3.edges filter (x => x.label == 'a')) )
  
  println("----------------------")
  val e = RDFGraph.empty
  val e1 = e.addTriples(Seq((IRI("1"),IRI("2"),IRI("3")),(IRI("1"),IRI("2"),IRI("4")),(IRI("2"),IRI("4"),IRI("5"))))
  println("Graph e1: " + e1)
  println("Triples e1: ")
  for {t <- e1.triples} println(t)
}