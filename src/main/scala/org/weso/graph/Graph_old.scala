package org.weso.graph

/* Graphs are represented as Functional Inductive Graphs 
 * following Martin Erwig approach */

/*
object Graph {

  case class Arrow(edge : NodeId, node: NodeId)
  
  type Arrows = Set[Arrow]

  case class NodeId(id : Int)

trait Graph[A] {
  def isEmpty : Boolean
  def contains(node : A) : Boolean 

  // TODO: Change signature to: insertNode(node: A): Graph[A] 
  def insertNode(node : A) : (Graph[A],NodeId)
  def insertTriple(source: A, edge: A, dest : A) : Graph[A]

  def findNode(id: NodeId) : Option[A]

  def getNode(id:NodeId) : A = {
    findNode(id) match {
      case None => throw new IllegalArgumentException("getNode: Node id(" + id + ") not found")
      case Some(n) => n
    }
  }


}

case class Empty[A]() extends Graph[A] {
  def isEmpty = true
  def findNode(id : NodeId) : Option[A] = None
  def contains(node : A) = false

  def insertNode(node : A) : (Graph[A],NodeId) = {
    val id = NodeId(1)
    (Cons(id,node,Set(),Set(),this),id)  
  }
n
  def insertTriple(source: A, edge: A, dest : A) : Graph[A] = {
    val (g1 ,id1) : (Graph[A],NodeId) = this.insertNode(source)
    val (g2 ,id2) : (Graph[A],NodeId) = g1.insertNode(edge)
    val id3 = NodeId(id2.id + 1)
    Cons(id3,dest,Set(Arrow(id2,id1)),Set(),g2)
  }

}

case class Cons[A](
		id : NodeId,     /* Id of node */
		content: A,      /* Content of node */
		in  : Arrows,    /* Incoming arrows */
		out : Arrows,    /* Outgoing arrows */
		rest : Graph[A]  /* Rest of the graph */
		) extends Graph[A] {

  def isEmpty = false

  def findNode (n: NodeId) : Option[A] = {
    if (id == n) Some(content)
    else rest.findNode(n)
  }
  
  def contains(node : A) : Boolean = {
    if (node == content) true
    else rest.contains(node)
  }
  
  def insertNode(node : A) : (Graph[A],NodeId) = {
    if (node == content || rest.contains(node)) 
      /* if node exists, we don't insert it...silently return its node id */
      (this,this.id)
    else {
      val newId = NodeId(id.id + 1)
      (Cons(newId,node,Set(),Set(),this),newId)
    }
  }
  
  def insertTriple(source: A, edge: A, dest : A) : Graph[A] = {
    val (g1,id1) : (Graph[A],NodeId) = this.insertNode(source)
    val (g2,id2) : (Graph[A],NodeId) = g1.insertNode(edge)
    if (g2.contains(dest)) {
     g2 /* TODO: update node dest with in arrows (id2,id1) */  
    }
    else {
        val id3 = NodeId(id.id + 1)
        Cons(id3,dest,Set(Arrow(id2,id1)),Set(),g2)
    }
  }

}

def triples[A] (graph : Graph[A]) : Set[(A,A,A)] = {
  graph match {
    case Empty() => Set.empty
    case Cons(id,node,in,out,rest) => triplesNode(node,in,out,graph) ++ triples(rest)
  }
}

def triplesNode[A] (node: A, in: Arrows, out : Arrows, graph: Graph[A]) : Set[(A,A,A)] = {
  (in.map(x => (graph.getNode(x.node), graph.getNode(x.edge), node)) 
      ++ 
   out.map(x => (node, graph.getNode(x.edge), graph.getNode(x.node)))
  )
}

}

*/