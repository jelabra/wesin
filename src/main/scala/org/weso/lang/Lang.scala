package org.weso.lang

/* An implementation of language identifiers following IETF RFC BCP 47 */

object Lang {
  type Lang = String 
}

  