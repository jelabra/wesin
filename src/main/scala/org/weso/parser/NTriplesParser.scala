package org.weso.parser

import scala.util.parsing.combinator.{Parsers, RegexParsers}
import scala.util.parsing.combinator.lexical.Lexical
import scala.util.parsing.input.Positional
import scala.util.parsing.input._
import util.parsing.input.CharSequenceReader.EofCh

import org.weso.rdfNode._
import org.weso.rdfTriple._

/**
 * NTriples Parser
 * http://www.w3.org/TR/rdf-testcases/#ntriples
 *
 * @author labra
 * @since 18/11/2012
 */

class NTriplesParser extends Positional with RegexParsers {
  val bNodesMap = scala.collection.mutable.Map.empty[String,BNodeId]
  
  override protected val whiteSpace = """(\s|\t)+""".r
  override val skipWhitespace = false

  
  def lazyrep[T] (p: Parser[Stream[T]]): Parser[Stream[T]] = (
    p ~ lazyrep(p) ^^ { case hd~tl => hd ++ tl }
    | success(Stream.empty)
  )

  def ntripleDoc : Parser[Stream[RDFTriple]] = {
    bNodesMap.clear()
    lazyrep(line) 
  }
  
  def line: Parser[Stream[RDFTriple]] =
    (comment | triple | eoln) ^^ {
      case Some(t) => Stream(t)
      case None => Stream.Empty
    }
  
  def triple : Parser[Option[RDFTriple]] = 
     opt(ws) ~> (subj <~ ws) ~ (pred <~ ws) ~ (obj <~ opt(ws)) <~ "." <~ opt(ws) ^^ 
       { case s ~ p ~ o => Some(RDFTriple(s,p,o)) } 
 
  def comment : Parser[Option[RDFTriple]] = 
    opt(ws) ~> """#[^\n\r]*""".r ~ eoln ^^ { case _ => None }

  
  def subj : Parser[RDFNode] = ( uriref | nodeID )
  def pred : Parser[IRI] = uriref
  def obj : Parser[RDFNode] = (uriref | nodeID | literal )
  
  def uriref : Parser[IRI] = "<" ~> absoluteURI <~ ">" ^^ IRI
  def nodeID : Parser[BNodeId] = "_:" ~> name ^^ 
    {(name) => 
     { bNodesMap.getOrElse(name, 
                      { val v = BNodeId(bNodesMap.size)
                      	bNodesMap.update(name,v);
                        v 
                      }) 
     }
    }
  def literal : Parser[Literal] = ( datatypeLiteral | langLiteral )
  
  def langLiteral : Parser[LangLiteral] = string ~ opt("@" ~> language) ^^ 
  		  { case str ~ Some(lang) => LangLiteral(str,lang)
  		  	case str ~ None       => LangLiteral(str,Lang(""))
  		  } 
  
  def datatypeLiteral : Parser[DatatypeLiteral] = string ~ "^^" ~ uriref ^^ 
		  { case str ~ "^^" ~ uri => DatatypeLiteral(str,uri) }
  
  def language : Parser[Lang] = """[a-z]+(-[a-z0-9]+)*""".r ^^ Lang
  
  def ws = """(\t|\s)+""".r
  
  def eof = """\z""".r
  
  def eoln = """\n|\r|\n\r""".r ^^ { case _ => None }
  def tab = """\t""".r 
  def string : Parser[String] = "\"" ~> rep(charSeq | chrExcept('\"', '\n', EofCh)) <~ "\"" ^^ { _ mkString "" }  
  
  def name: Parser[String] = """[A-Za-z][A-Za-z0-9]*""".r 
  def absoluteURI : Parser[String] = rep(charURI | chrExcept('>', '\n', EofCh) ) ^^ { _ mkString "" }

  def charURI = elem("URI char", 
      (ch) => ch != '<' && ch != '>' 
        				&& ch > 0x32 // Not allowed control and space chars in URIs
        				&& ch <= 0x7F // Only ASCII (chars < 127) in URIs 
        				) 
        				
  def character = elem("character", (ch) => ch >= 20 && ch <= 126)

  def charSeq: Parser[String] =
    ('\\' ~ '\"' ^^^ "\""
    |'\\' ~ '\\' ^^^ "\\"
    |'\\' ~ '/'  ^^^ "/"
    |'\\' ~ 'b'  ^^^ "\b"
    |'\\' ~ 'f'  ^^^ "\f"
    |'\\' ~ 'n'  ^^^ "\n"
    |'\\' ~ 'r'  ^^^ "\r"
    |'\\' ~ 't'  ^^^ "\t"
    |'\\' ~> 'u' ~> unicodeBlock)

  private def unicodeBlock = hexDigit ~ hexDigit ~ hexDigit ~ hexDigit ^^ {
    case a ~ b ~ c ~ d =>
      new String(Array(Integer.parseInt(List(a, b, c, d) mkString "", 16)), 0, 1)
  }
  
  val hexDigits = Set[Char]() ++ "0123456789abcdefABCDEF".toArray
  def hexDigit = elem("hex digit", hexDigits.contains(_))

  def chrExcept(cs: Char*) = elem("", ch => (cs forall (ch !=)))
}

object NTriplesParser extends NTriplesParser {
  def main(args: Array[String]) {
    
	  val t1 = "<a> <b> <c> .\n"
	  println("t1: " + parseAll(line, t1))

	  val t2 = "# Comment \n"
	  println("t2: " + parseAll(line, t2))
	  
	  val t3 = "\n"
	  println("t3: " + parseAll(line, t3))

 
	  val ts = """# a comment 
	             |<a> <b> <c> .
	             |<a> <b> <d> .""".stripMargin('|')
	             
	  val t4 =
"""# Example 3 of N-Triples with bNodes
<http://example.org/a> <http://example.org/b> _:b1 . 
<http://example.org/a> <http://example.org/b> _:b2 . 
_:b1 <http://example.org/b> <http://example.org/c> . 
_:b1 <http://example.org/b> <http://example.org/d> . 
_:b2 <http://example.org/b> _:b1 . 
_:b2 <http://example.org/b> <http://example.org/e> . """.stripMargin

	  val triples = parseAll(ntripleDoc, t4).get
	  println("Parsed")
	  for { t <- triples } { println(t) }
	}
}