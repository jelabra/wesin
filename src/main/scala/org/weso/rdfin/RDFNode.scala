package org.weso.rdfin

import org.weso.iri.IRI._
import org.weso.lang.Lang._

case class BNodeId(id : String)

class RDFNode {
  
}


case class Resource(iri : IRI) extends RDFNode {
  
}

case class Literal(value : String, datatypeIRI : IRI, lang : Lang) extends RDFNode {
  
}

case class BNode(id : BNodeId) extends RDFNode {
  
}
