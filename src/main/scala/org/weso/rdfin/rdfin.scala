package org.weso.rdfin

import org.weso.rdfGraph
import org.weso.rdfGraph._
import org.weso.rdfNode._
import org.weso.rdfTriple._

object RDFIn extends App {

 override def main(args: Array[String]) = {
   println("Inductive RDF")
   val g = RDFGraph.empty
   val g1 = g.insertTriple(RDFTriple(IRI("1"),IRI("2"),IRI("3")))
   val g2 = g1.insertTriple(RDFTriple(IRI("1"),IRI("2"),IRI("4")))
   for {
     t <- g2.triples(BNodeId(50))
   } println(t)
 }

}

