package org.weso.triple

import org.weso.rdfNode._
import scala.collection.Set

case class Triple(subj : RDFNode, pred : IRI, obj : RDFNode) {
  def extractBNode (node: RDFNode) : Set[BNodeId] = {
    node match {
      case b@BNodeId(id) => Set(b)
      case _ => Set()
    }
  }
    
  def bNodes : Set[BNodeId] = {
    val set = Set()
    set ++ extractBNode(subj) ++ extractBNode(obj)
  }
}

object Triple {

/**
 * collects BNodes in a set of triples
 */
def collectBNodes (triples : Set[Triple]) : Set[BNodeId] = {
    triples.foldLeft (Set[BNodeId]()) ((set,triple) => 
    										set ++ triple.bNodes)
}	

}



