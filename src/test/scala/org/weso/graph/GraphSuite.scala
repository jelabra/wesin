package org.weso.graph

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.weso.graph.Graph._

@RunWith(classOf[JUnitRunner])
class GraphSuite extends FunSuite {

  test("empty graph") {
    val g = Graph.Empty()
    assert(g.isEmpty,true)
  }
  
  test("simple triple") {
    val g0 = Empty[Char]()
    val g1 = Cons(NodeId(1),'A',Set(),Set(),g0)
    val g2 = Cons(NodeId(2),'B',Set(),Set(),g1)
    val g3 = Cons(NodeId(3),'C',Set(Arrow(NodeId(2),NodeId(1))),Set(),g2)
    assert(triples(g3).contains(('A','B','C')))
  }

  test("two triples") {
    val g0 = Empty[Char]()
    val g1 = Cons(NodeId(1),'A',Set(),Set(),g0)
    val g2 = Cons(NodeId(2),'B',Set(),Set(),g1)
    val g3 = Cons(NodeId(3),'C',Set(Arrow(NodeId(2),NodeId(1))),Set(),g2)
    val g4 = Cons(NodeId(4),'D',Set(),Set(Arrow(NodeId(2),NodeId(1))),g3)
    assert(triples(g4).size==2)
    assert(triples(g4).contains(('A','B','C')))
    assert(triples(g4).contains(('D','B','A')))
  }

}
