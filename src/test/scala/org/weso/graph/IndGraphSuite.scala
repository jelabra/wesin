package org.weso.graph

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.weso.graph.IndGraph._

@RunWith(classOf[JUnitRunner])
class GraphSuite extends FunSuite {

  test("empty graph") {
    val g = IndGraph.Empty()
    assert(g.isEmpty,true)
  }
  
  test("simple triple contains origin") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    assert(g1.nodes.contains('a'))
  }

  test("simple triple contains edge") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    assert(g1.nodes.contains('b'))
  }

  test("simple triple contains destiny") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    assert(g1.nodes.contains('c'))
  }


  test("match with two triples, a") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    val g2 = g1.insert('a','b','d')
    assert(g2.matchGraph('a').get._1.pred === Seq())
    assert(g2.matchGraph('a').get._1.succ === Seq(Arrow('b','c'),Arrow('b','d')))
    assert(g2.matchGraph('a').get._1.nodes === Seq())
  }

  test("match with two triples, b") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    val g2 = g1.insert('a','b','d')
    assert(g2.matchGraph('b').get._1.pred === Seq())
    assert(g2.matchGraph('b').get._1.succ === Seq())
    assert(g2.matchGraph('b').get._1.nodes === Seq(('a','c'),('a','d')))
  }

  test("match with two triples, c") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    val g2 = g1.insert('a','b','d')
    assert(g2.matchGraph('c').get._1.pred === Seq(Arrow('b','a')))
    assert(g2.matchGraph('c').get._1.succ === Seq())
    assert(g2.matchGraph('c').get._1.nodes === Seq())
  }

    test("match triples. no order") {
    val g0 = Empty[Char]()
    val g1 = g0.insert('a','b','c')
    val g2 = g1.insert('a','b','d')
    
    assert(g2.triples.toSet === Set(('a','b','d'),('a','b','c')))
  }

}
